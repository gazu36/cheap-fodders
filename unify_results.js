const fs = require('fs');
const fsp = require('fs').promises;
const Heap = require('heap');

const sbc_maps = [
    {
        sbc_name: "The Challenger",
        group: "League and Nation Hybrid",
        id: 33,
    },
    {
        sbc_name: "Advanced",
        group: "League and Nation Hybrid",
        id: 34,
    },
    {
        sbc_name: "Fiendsh",
        group: "League and Nation Hybrid",
        id: 35,
    },
    {
        sbc_name: "Puzzle Master",
        group: "League and Nation Hybrid",
        id: 36,
    },
    {
        sbc_name: "Give Me Five",
        group: "Hybrid Leagues",
        id: 17,
    },
    {
        sbc_name: "Seven League Boots",
        group: "Hybrid Leagues",
        id: 18,
    },
    {
        sbc_name: "The Whole Nine Yards",
        group: "Hybrid Leagues",
        id: 19,
    },
    {
        sbc_name: "First XI",
        group: "Hybrid Leagues",
        id: 20,
    },
    {
        sbc_name: "The Final Four",
        group: "Hybrid Nations",
        id: 25,
    },
    {
        sbc_name: "Six of the Best",
        group: "Hybrid Nations",
        id: 26,
    },
    {
        sbc_name: "Elite Eight",
        group: "Hybrid Nations",
        id: 27,
    },
    {
        sbc_name: "Around the World",
        group: "Hybrid Nations",
        id: 28,
    },
]

const _dumpToFile = async (filename, ext, body, encoding = 'utf8') => fsp.writeFile(`${ext}/${filename}.${ext}`, body, encoding);

const dumpJsonToFile = async (filename, json) => _dumpToFile(filename, 'json', JSON.stringify(json, null, 2));

const dumpTextToFile = async(filename, text) => _dumpToFile(filename, 'txt', text);

const playerComparator = (a, b) => a.count - b.count;

const sbcFiles = fs.readdirSync('./json').filter(file => file.endsWith('.json') && !file.startsWith('__'));

const prettyPrintPlayerData = (playerData) => `${playerData.name} ${playerData.position} (${playerData.rating}, ${playerData.rarity} ${playerData.version}) - ${playerData.count} occurences`;

const hashPlayerData = (playerData) => `${playerData.player_id}-${playerData.position}`;

let counterMap = {}

const updatePlayerInCounterMap = async (player) => {
    let currHash = hashPlayerData(player);
    if (!(currHash in counterMap)) {
        counterMap[currHash] = {
            ...player,
            count: 0
        }
    }
    counterMap[currHash].count += player.count;
}

const updateSBCInCounterMap = async (players) => players.map(updatePlayerInCounterMap)

const sumAll = async (sbc_files) => {
    for (const sbc_file of sbc_files) {
        const json_data = require(`./json/${sbc_file}`);
        updateSBCInCounterMap(json_data.players);
    }
}

const unify = async () => {
    sumAll(sbcFiles);

    let ordered = Heap.nlargest(Object.values(counterMap), Object.keys(counterMap).length, playerComparator);
    await Promise.all([
        dumpJsonToFile('__all', {write_ts: new Date().toISOString(), players: ordered}),
        dumpTextToFile('__all', ordered.map(prettyPrintPlayerData).join('\n')),
    ]);
}


(async () => {
    await unify();
})();