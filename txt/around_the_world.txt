Gosens LWB (82, rare gold) - 31 occurences
Hateboer RWB (80, rare gold) - 24 occurences
Djimsiti CB (79, rare gold) - 24 occurences
Pašalić CAM (81, rare gold) - 22 occurences
Ibrahimović ST (82, rare gold) - 22 occurences
Lobotka CM (81, rare gold) - 19 occurences
Luis Alberto CM (83, rare gold) - 18 occurences
Rafael Tolói CB (81, rare gold) - 14 occurences
Żurkowski CM (76, rare gold) - 12 occurences
De Ketelaere ST (78, rare gold) - 12 occurences
Musso GK (81, rare gold) - 10 occurences
Demiral CB (77, common gold) - 10 occurences
Igor CB (76, common gold) - 9 occurences
Martínez Quarta CB (76, common gold) - 9 occurences
Rrahmani CB (80, common gold) - 8 occurences
Nélson Semedo RWB (80, rare gold) - 8 occurences
Aguerd CB (80, rare gold) - 7 occurences
Juan Jesus CB (76, common gold) - 7 occurences
Rui Patrício GK (82, rare gold) - 7 occurences
Onana GK (82, rare gold) - 7 occurences
Handanovič GK (84, rare gold) - 6 occurences
Mitrović ST (78, rare gold) - 6 occurences
Abraham ST (82, rare gold) - 6 occurences
Tapsoba CB (81, rare gold) - 6 occurences
Dumfries RWB (82, rare gold) - 6 occurences
de Vrij CB (84, rare gold) - 6 occurences
Matip CB (84, rare gold) - 6 occurences
Fabiański GK (82, rare gold) - 6 occurences
Angeliño LWB (83, rare gold) - 6 occurences
Haller ST (82, rare gold) - 6 occurences
Zapata ST (83, rare gold) - 5 occurences
Tielemans CM (84, rare gold) - 5 occurences
Dia ST (79, common gold) - 5 occurences
Trimmel RWB (77, common gold) - 5 occurences
Sánchez CB (79, rare gold) - 5 occurences
Coutinho CAM (82, rare gold) - 4 occurences
Pessina CAM (77, common gold) - 4 occurences
Keïta CM (81, rare gold) - 4 occurences
Vecino CM (77, common gold) - 4 occurences
Mavropanos CB (78, rare gold) - 4 occurences
Quagliarella ST (76, common gold) - 4 occurences
Borré ST (78, rare gold) - 4 occurences
Henry ST (76, common gold) - 4 occurences
Demirbay CM (81, rare gold) - 4 occurences
Zappacosta LWB (79, common gold) - 3 occurences
Pobega CM (76, rare gold) - 3 occurences
Simeone ST (78, rare gold) - 3 occurences
Faraoni RWB (78, common gold) - 3 occurences
Hrádecký GK (83, rare gold) - 3 occurences
Bonucci CB (84, rare gold) - 3 occurences
Casteels GK (84, rare gold) - 3 occurences
Chilwell LWB (82, rare gold) - 3 occurences
Elmas CAM (78, common gold) - 3 occurences
Jonny LWB (79, common gold) - 3 occurences
Romagnoli CB (80, common gold) - 3 occurences
Aït Nouri LWB (76, common gold) - 3 occurences
Rúben Neves CM (83, rare gold) - 2 occurences
Jiménez ST (82, common gold) - 2 occurences
Milik ST (79, common gold) - 2 occurences
Bastoni CB (84, rare gold) - 2 occurences
Arnautović ST (82, rare gold) - 2 occurences
Bruno Guimarães CM (81, rare gold) - 2 occurences
Bentancur CM (79, rare gold) - 2 occurences
Maddison CAM (82, rare gold) - 2 occurences
Szoboszlai CAM (79, rare gold) - 2 occurences
Çalhanoğlu CM (84, rare gold) - 2 occurences
Widmer RWB (77, common gold) - 2 occurences
Niakhaté CB (78, rare gold) - 2 occurences
Simakan CB (78, rare gold) - 2 occurences
Arthur Cabral ST (77, common gold) - 2 occurences
Mateta ST (76, common gold) - 2 occurences
Wijnaldum CM (80, rare gold) - 2 occurences
Svanberg CM (76, common gold) - 2 occurences
Antonio ST (79, rare gold) - 2 occurences
Endo CM (78, rare gold) - 2 occurences
Diallo CB (77, common gold) - 2 occurences
Brahim CAM (78, rare gold) - 2 occurences
Bajrami CAM (76, common gold) - 2 occurences
Struijk CB (76, common gold) - 2 occurences
Bonaventura CM (78, common gold) - 2 occurences
Forsberg CAM (81, rare gold) - 2 occurences
Shelvey CM (77, common gold) - 1 occurences
Tuta CB (77, common gold) - 1 occurences
Kalajdžić ST (78, common gold) - 1 occurences
Kampl CM (80, common gold) - 1 occurences
Maggiore CM (77, common gold) - 1 occurences
José Sá GK (81, rare gold) - 1 occurences
Ginter CB (82, common gold) - 1 occurences
Frattesi CM (77, common gold) - 1 occurences
Vogt CB (76, common gold) - 1 occurences
Salisu CB (77, common gold) - 1 occurences
Kalulu CB (78, rare gold) - 1 occurences
Raum LWB (81, rare gold) - 1 occurences
Hummels CB (84, rare gold) - 1 occurences
Sabitzer CM (80, rare gold) - 1 occurences
Brandt CAM (82, rare gold) - 1 occurences
Adams ST (76, common gold) - 1 occurences
Diego Llorente CB (76, common gold) - 1 occurences
Rodrigo CAM (77, common gold) - 1 occurences
Freuler CM (80, common gold) - 1 occurences
Skorupski GK (76, common gold) - 1 occurences
Wood ST (76, common gold) - 1 occurences
Henderson CM (83, rare gold) - 1 occurences
Bebou ST (77, rare gold) - 1 occurences
Reyna CAM (77, common gold) - 1 occurences
Miranchuk CAM (78, common gold) - 1 occurences
Smalling CB (81, common gold) - 1 occurences
Edouard ST (76, rare gold) - 1 occurences
Ndicka CB (81, rare gold) - 1 occurences
Tah CB (82, rare gold) - 1 occurences
Kamada CAM (80, common gold) - 1 occurences
Mancini CB (81, common gold) - 1 occurences
Kobel GK (83, rare gold) - 1 occurences
Konsa CB (79, rare gold) - 1 occurences
Ilić CM (76, common gold) - 1 occurences
Faes CB (77, common gold) - 1 occurences
Mount CAM (84, rare gold) - 1 occurences
Singo RWB (76, common gold) - 1 occurences
Haidara CM (78, common gold) - 1 occurences
Günter CB (76, common gold) - 1 occurences
Audero GK (78, common gold) - 1 occurences
Ndombele CM (79, common gold) - 1 occurences
Umtiti CB (79, common gold) - 1 occurences
Musiala CM (81, rare gold) - 1 occurences
Gerhardt CM (76, common gold) - 1 occurences
Guirassy ST (76, common gold) - 1 occurences
Awoniyi ST (77, common gold) - 1 occurences
Baumgartl CB (76, common gold) - 1 occurences
Krunić CM (77, common gold) - 1 occurences
Stones CB (83, rare gold) - 1 occurences
Maupay ST (76, common gold) - 1 occurences
Lukić CM (77, common gold) - 1 occurences
Gollini GK (78, common gold) - 1 occurences
Sow CM (80, rare gold) - 1 occurences
Azmoun ST (80, rare gold) - 1 occurences
Acerbi CB (82, common gold) - 1 occurences
André Silva ST (82, rare gold) - 1 occurences
Turner GK (77, common gold) - 1 occurences
Luís Maximiano GK (79, common gold) - 1 occurences
Praet CAM (78, common gold) - 1 occurences
Džeko ST (84, rare gold) - 1 occurences
Strakosha GK (78, common gold) - 1 occurences
Guilbert RWB (78, common gold) - 1 occurences
Rugani CB (76, common gold) - 1 occurences