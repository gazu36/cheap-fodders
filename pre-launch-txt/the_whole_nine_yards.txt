Pedro Chirivella Burgos CDM (77, rare gold) - 19 occurences
Mario Gaspar RB (78, rare gold) - 19 occurences
Marc Bartra Aregall CB (81, rare gold) - 19 occurences
Gerard Deulofeu Lázaro CF → ST (80, rare gold) - 17 occurences
Lucas Alario ST (78, rare gold) - 13 occurences
Nicolás Otamendi CB (81, rare gold) - 13 occurences
Juan Foyth RB (79, rare gold) - 11 occurences
Luiz Araújo RM (78, rare gold) - 10 occurences
Franco Cervi LM (78, rare gold) - 9 occurences
Walter Benítez GK (80, rare gold) - 9 occurences
Leandro Paredes CDM (80, rare gold) - 9 occurences
Darío Benedetto ST (77, rare gold) - 9 occurences
Pedro Porro RWB → RM (81, rare gold) - 9 occurences
Exequiel Palacios CDM (78, rare gold) - 9 occurences
David López Silva CDM (77, rare gold) - 8 occurences
Fernando Lucas Martins CDM (78, rare gold) - 8 occurences
Norberto Murara Neto GK (81, rare gold) - 8 occurences
Ibrahim Sangaré CDM (81, rare gold) - 8 occurences
Emiliano Rigoni RM (78, rare gold) - 8 occurences
Emiliano Rigoni RM → LM (78, rare gold) - 8 occurences
Lisandro Martínez CB (81, rare gold) - 8 occurences
Azpilicueta CB (82, rare gold) - 8 occurences
De Frutos RM (77, rare gold) - 8 occurences
Nicolás Tagliafico LB (81, rare gold) - 7 occurences
Kepa Arrizabalaga GK (80, rare gold) - 7 occurences
Emiliano Buendía RM (80, rare gold) - 7 occurences
Richarlison de Andrade ST (81, rare gold) - 7 occurences
Éver Banega CM → CDM (81, rare gold) - 7 occurences
Rodrygo Silva de Goes RW (81, rare gold) - 7 occurences
Antonio Adán Garrido GK (81, rare gold) - 6 occurences
Bruno Viana CB (77, rare gold) - 6 occurences
Gilberto Moraes Júnior RB (78, rare gold) - 5 occurences
Danilo Luiz da Silva RB (80, rare gold) - 5 occurences
Milton Casco LB → RB (77, rare gold) - 5 occurences
Ángel Montoro Sánchez CDM (77, rare gold) - 5 occurences
Grimaldo LB (82, rare gold) - 5 occurences
Andrei Girotto CB (78, rare gold) - 5 occurences
Anderson Talisca CF → ST (82, rare gold) - 4 occurences
Raúl García Escudero ST (80, rare gold) - 4 occurences
Carlos Izquierdoz CB (77, rare gold) - 4 occurences
Juan Bernat Velasco LB (78, rare gold) - 4 occurences
Arthur Mendonça Cabral ST (77, rare gold) - 4 occurences
Pepê LM (77, rare gold) - 4 occurences
Víctor Machín Pérez LM → ST (77, rare gold) - 4 occurences
Juan Musso GK (81, rare gold) - 4 occurences
Taremi ST (81, rare gold) - 4 occurences
Iago Amaral Borduchi LWB → LM (77, rare gold) - 4 occurences
Jorge Molina Vidal ST (78, rare gold) - 4 occurences
Toby Alderweireld CB (82, rare gold) - 4 occurences
Lisandro Martínez CB → LB (81, rare gold) - 3 occurences
Robin Le Normand CB (81, rare gold) - 3 occurences
Sergio Romero GK (78, rare gold) - 3 occurences
Olivier Boscagli CB (77, rare gold) - 3 occurences
Nacho Fernández CB (82, rare gold) - 3 occurences
Kasper Schmeichel GK (83, rare gold) - 3 occurences
Robert Lynch Sanchéz GK (77, rare gold) - 3 occurences
Víctor Machín Pérez LM (77, rare gold) - 3 occurences
Guerreiro LB (82, rare gold) - 3 occurences
Ismaily LB (79, rare gold) - 3 occurences
Iago Amaral Borduchi LWB → LB (77, rare gold) - 3 occurences
Valentin Rosier RB (77, rare gold) - 3 occurences
Jean Butez GK (77, rare gold) - 3 occurences
Jean-David Beauguel ST (77, rare gold) - 3 occurences
Allan Saint-Maximin LM (81, rare gold) - 3 occurences
Oriol Romeu Vidal CDM (78, rare gold) - 3 occurences
Davidson da Luz Pereira LM (77, rare gold) - 2 occurences
Arthur CM → CDM (80, rare gold) - 2 occurences
Randal Kolo Muani ST → RM (78, rare gold) - 2 occurences
Boubacar Kamara CDM (80, rare gold) - 2 occurences
Giovanni Simeone ST (78, rare gold) - 2 occurences
Olivier Giroud ST (82, rare gold) - 2 occurences
Lisandro Martínez CB → CDM (81, rare gold) - 2 occurences
Marcos Rojo CB (78, rare gold) - 2 occurences
Hugo Duro Perales ST (77, rare gold) - 2 occurences
Jeremie Frimpong RB (80, rare gold) - 2 occurences
Noa Lang LW → ST (78, rare gold) - 2 occurences
Bruno Guimarães Moura CM → CDM (81, rare gold) - 2 occurences
Mario Hermoso Canseco CB (80, rare gold) - 2 occurences
Pablo Rosario CDM (78, rare gold) - 2 occurences
Pedro Rodríguez Ledesma LW → LM (79, rare gold) - 2 occurences
Marten de Roon CDM (80, rare gold) - 2 occurences
Matheus Cunha ST (81, rare gold) - 2 occurences
Germán CB (77, rare gold) - 2 occurences
Matheus Lima Magalhães GK (79, rare gold) - 2 occurences
Lukáš Hrádecký GK (83, rare gold) - 2 occurences
Lucas Martínez Quarta CB (76, non-rare gold) - 2 occurences
Borja Iglesias Quintas ST (81, rare gold) - 2 occurences
Seko Fofana CM (81, rare gold) - 2 occurences
Zubimendi CM → CDM (79, rare gold) - 2 occurences
Pablo Fornals Malla CAM → LM (79, rare gold) - 2 occurences
Igor CB (76, non-rare gold) - 2 occurences
Denzel Dumfries RWB → RM (82, rare gold) - 2 occurences
Jeremiah St. Juste CB (76, rare gold) - 2 occurences
Wout Weghorst ST (79, rare gold) - 2 occurences
Jasper Cillessen GK (80, rare gold) - 2 occurences
Lucas Pérez Martínez ST (78, rare gold) - 2 occurences
Marc Cucurella Saseta LB (81, rare gold) - 2 occurences
Antoine Griezmann ST (83, rare gold) - 2 occurences
Enzo Pérez CDM (78, rare gold) - 2 occurences
Gerrit Holtmann LW (74, rare silver) - 2 occurences
Pau López Sabata GK (79, rare gold) - 2 occurences
Marcão CB (79, rare gold) - 1 occurences
Mattéo Guendouzi CM (80, rare gold) - 1 occurences
Felipe CB (81, rare gold) - 1 occurences
Brais Méndez Portela RM (77, rare gold) - 1 occurences
Aitor Fernández GK (77, rare gold) - 1 occurences
Matteo Politano RW (81, rare gold) - 1 occurences
Fernando Reges CDM (83, rare gold) - 1 occurences
Willian Arao CDM (77, rare gold) - 1 occurences
Gabriel Armando de Abreu CB (82, rare gold) - 1 occurences
Christian Pulisic LW (82, rare gold) - 1 occurences
Maxime Lopez CM → CDM (78, rare gold) - 1 occurences
Lucas Digne LB (82, rare gold) - 1 occurences
Josuha Guilavogui CDM (75, non-rare gold) - 1 occurences
Unai Vencedor Paris CM → CDM (77, rare gold) - 1 occurences
Ferrán Torres LW → ST (82, rare gold) - 1 occurences
Davidson da Luz Pereira LM → ST (77, rare gold) - 1 occurences
Lucas Silva Melo CB (77, rare gold) - 1 occurences
Gerard Piqué Bernabeu CB (83, rare gold) - 1 occurences
Mario Hermoso Canseco CB → LB (80, rare gold) - 1 occurences
Bartol Franjic CDM (71, non-rare silver) - 1 occurences
Raúl Albiol Tortajada CB (82, rare gold) - 1 occurences
Rafał Gikiewicz GK (77, rare gold) - 1 occurences
Pape Gueye CDM (78, non-rare gold) - 1 occurences
Kevin Schade RM (70, rare silver) - 1 occurences
Jordan Veretout CDM (79, rare gold) - 1 occurences
Owen Wijndal LB (79, rare gold) - 1 occurences
Nathan Aké CB (78, rare gold) - 1 occurences
Tyrell Malacia LB (79, rare gold) - 1 occurences
Bremer CB (83, rare gold) - 1 occurences
Fran Navarro ST (75, non-rare gold) - 1 occurences
Djibril Sow CM (80, rare gold) - 1 occurences
Nahuel Molina RWB → RM (78, rare gold) - 1 occurences
Alex Nicolao Telles LB (80, rare gold) - 1 occurences
Víctor Ruiz Torre CB (78, rare gold) - 1 occurences
Vallejo CB (75, non-rare gold) - 1 occurences
Guido Rodríguez CDM (81, rare gold) - 1 occurences
Jordan Ferri CDM (77, rare gold) - 1 occurences
Aurélien Tchouaméni CM (82, rare gold) - 1 occurences
Ferrán Torres LW (82, rare gold) - 1 occurences
Rodrigo Moreno Machado CAM → ST (77, rare gold) - 1 occurences
Daley Blind CB (80, rare gold) - 1 occurences
Denzel Dumfries RWB (82, rare gold) - 1 occurences
Portu RM (78, rare gold) - 1 occurences
Alex Sandro Lobo Silva LB (80, rare gold) - 1 occurences
Germán Pezzella CB (77, rare gold) - 1 occurences
Unai Núñez Gestoso CB (79, rare gold) - 1 occurences
Gabriel Jesus ST (83, rare gold) - 1 occurences
Mauro Icardi ST (77, rare gold) - 1 occurences
Ruslan Malinovskyi CF → ST (82, rare gold) - 1 occurences
Caio Henrique LB (78, rare gold) - 1 occurences
Paulo Otávio LB (76, non-rare gold) - 1 occurences