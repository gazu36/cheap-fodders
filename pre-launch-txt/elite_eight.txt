Pep Biel Mas Jaume CAM (75, non-rare gold) - 37 occurences
Michel Aebischer CM (75, non-rare gold) - 28 occurences
Mathías Olivera LB (77, rare gold) - 25 occurences
Nicolás Domínguez CM (76, non-rare gold) - 23 occurences
Jacob Ramsey CM (75, non-rare gold) - 22 occurences
Stefan Posch CB (75, non-rare gold) - 22 occurences
Cesinha LW (75, non-rare gold) - 20 occurences
Sergiño Dest RB (77, rare gold) - 19 occurences
Paulo Otávio LB (76, non-rare gold) - 18 occurences
Riccardo Saponara LW (76, non-rare gold) - 17 occurences
Pietro Terracciano GK (76, non-rare gold) - 17 occurences
Igor CB (76, non-rare gold) - 16 occurences
Nicolò Casale CB (76, non-rare gold) - 15 occurences
Gaetano Castrovilli CM (77, rare gold) - 11 occurences
Maxime Lopez CM (78, rare gold) - 10 occurences
Alexis Saelemaekers RW (78, rare gold) - 10 occurences
Łukasz Skorupski GK (76, non-rare gold) - 10 occurences
Giovanni Simeone ST (78, rare gold) - 9 occurences
Miguel Veloso CM (77, rare gold) - 8 occurences
Nathan Collins CB (75, non-rare gold) - 7 occurences
Daniel-Kofi Kyereh CAM (75, non-rare gold) - 7 occurences
Sebastiaan Bornauw CB (75, non-rare gold) - 7 occurences
Callum Hudson-Odoi LW (77, rare gold) - 7 occurences
Angelo Fulgini CM (77, rare gold) - 6 occurences
Junior Messias RW (78, rare gold) - 6 occurences
Konstantinos Tsimikas LB (77, rare gold) - 6 occurences
Vladimír Darida CM (75, non-rare gold) - 6 occurences
Arthur Mendonça Cabral ST (77, rare gold) - 6 occurences
Lucas Martínez Quarta CB (76, non-rare gold) - 5 occurences
Pascal Groß CM (77, rare gold) - 5 occurences
Illan Meslier GK (77, rare gold) - 5 occurences
Georginio Rutter ST (75, non-rare gold) - 4 occurences
Hee Chan Hwang LW (77, rare gold) - 4 occurences
Deniz Undav ST (77, rare gold) - 4 occurences
Ko Itakura CB (75, rare gold) - 4 occurences
Merih Demiral CB (77, rare gold) - 4 occurences
Matias Vecino CM (77, rare gold) - 4 occurences
Waldemar Anton CB (75, non-rare gold) - 4 occurences
Roberto Pereyra CM (78, rare gold) - 4 occurences
Marco Davide Faraoni RWB → RB (78, rare gold) - 4 occurences
Tino Livramento RB (75, non-rare gold) - 4 occurences
Mário Rui Silva Duarte LB (78, rare gold) - 4 occurences
Jonathan Ikoné RW (78, rare gold) - 3 occurences
Nicolas Seiwald CM (75, non-rare gold) - 3 occurences
Yannick Gerhardt CM (76, non-rare gold) - 3 occurences
Rafał Gikiewicz GK (77, rare gold) - 3 occurences
Ritsu Doan RM → RW (77, rare gold) - 3 occurences
Mattias Svanberg CM (76, non-rare gold) - 3 occurences
Marius Wolf RM → RW (77, rare gold) - 3 occurences
Alexander Schwolow GK (75, non-rare gold) - 3 occurences
Warmed Omari CB (75, non-rare gold) - 3 occurences
Wout Faes CB (77, rare gold) - 3 occurences
Issa Diop CB (77, rare gold) - 3 occurences
Alex Oxlade-Chamberlain CM (77, rare gold) - 2 occurences
Roger Ibañez Da Silva CB (79, rare gold) - 2 occurences
Vallejo CB (75, non-rare gold) - 2 occurences
Jérôme Roussillon LB (76, non-rare gold) - 2 occurences
Alex Sandro Lobo Silva LB (80, rare gold) - 2 occurences
Patson Daka ST (77, rare gold) - 2 occurences
John McGinn CM (79, rare gold) - 2 occurences
Saša Lukić CM (77, rare gold) - 2 occurences
Christopher Trimmel RWB → RB (77, rare gold) - 2 occurences
Jérémie Boga LW (77, rare gold) - 2 occurences
Adama Traoré Diarra RW (79, rare gold) - 2 occurences
Douglas Luiz CDM → CM (78, rare gold) - 2 occurences
Lucas Alario ST (78, rare gold) - 2 occurences
Lucas Rodrigues M. Silva RW (80, rare gold) - 2 occurences
Robert Lynch Sanchéz GK (77, rare gold) - 2 occurences
Matt Doherty RWB → RB (77, rare gold) - 2 occurences
Djibril Sow CM (80, rare gold) - 1 occurences
Pedro Chirivella Burgos CDM → CM (77, rare gold) - 1 occurences
Danilo Luiz da Silva RB (80, rare gold) - 1 occurences
Rodrigo Bentancur CM (79, rare gold) - 1 occurences
Habib Diallo ST (77, rare gold) - 1 occurences
Charles De Ketelaere ST → LW (78, rare gold) - 1 occurences
Emiliano Rigoni RM → RW (78, rare gold) - 1 occurences
Ludwig Augustinsson LB (77, rare gold) - 1 occurences
Nathan Aké CB (78, rare gold) - 1 occurences
Ihlas Bebou ST → RW (77, rare gold) - 1 occurences
Gerson Santos da Silva CM (78, rare gold) - 1 occurences
Cédric Brunner RB (75, non-rare gold) - 1 occurences
Darko Lazović LM → LW (77, rare gold) - 1 occurences
Gerard Deulofeu Lázaro CF → ST (80, rare gold) - 1 occurences
Charles De Ketelaere ST (78, rare gold) - 1 occurences
Wataru Endo CM (78, rare gold) - 1 occurences
Julian Weigl CM (79, rare gold) - 1 occurences
Josip Šutalo CB (75, non-rare gold) - 1 occurences
James Tarkowski CB (79, rare gold) - 1 occurences
Malo Gusto RB (75, rare gold) - 1 occurences
Adrien Thomasson CM (77, rare gold) - 1 occurences
Jiří Pavlenka GK (78, rare gold) - 1 occurences
Ademola Lookman LM → LW (77, rare gold) - 1 occurences
Weston McKennie CM (80, rare gold) - 1 occurences
Miguel Almirón RM → RW (78, rare gold) - 1 occurences
Moise Kean ST (78, rare gold) - 1 occurences
Alexander Djiku CB (77, rare gold) - 1 occurences
Kelechi Iheanacho ST (77, rare gold) - 1 occurences
Oliver Skipp CDM → CM (77, rare gold) - 1 occurences
Moses Simon LM → LW (77, rare gold) - 1 occurences
Bruno Petković ST (75, non-rare gold) - 1 occurences