const fs = require('fs').promises;
const { gotScraping } = require('got-scraping');
const Heap = require('heap');
const { MultiProgressBars } = require('multi-progress-bars');
const conf = require('./conf.json');

const sbc_maps = [
    {
        sbc_name: "The Challenger",
        group: "League and Nation Hybrid",
        id: 25,
    },
    {
        sbc_name: "Advanced",
        group: "League and Nation Hybrid",
        id: 26,
    },
    {
        sbc_name: "Fiendsh",
        group: "League and Nation Hybrid",
        id: 27,
    },
    {
        sbc_name: "Puzzle Master",
        group: "League and Nation Hybrid",
        id: 28,
    },
    {
        sbc_name: "Give Me Five",
        group: "Hybrid Leagues",
        id: 17,
    },
    {
        sbc_name: "Seven League Boots",
        group: "Hybrid Leagues",
        id: 18,
    },
    {
        sbc_name: "The Whole Nine Yards",
        group: "Hybrid Leagues",
        id: 19,
    },
    {
        sbc_name: "First XI",
        group: "Hybrid Leagues",
        id: 20,
    },
    {
        sbc_name: "The Final Four",
        group: "Hybrid Nations",
        id: 21,
    },
    {
        sbc_name: "Six of the Best",
        group: "Hybrid Nations",
        id: 22,
    },
    {
        sbc_name: "Elite Eight",
        group: "Hybrid Nations",
        id: 23,
    },
    {
        sbc_name: "Around the World",
        group: "Hybrid Nations",
        id: 24,
    },
]

let seen_solutions = require('./seen_solutions.json');

const playerComparator = (a, b) => a.count - b.count;

const extractPlayerData = (playerItem) => ({
    name: playerItem.name,
    player_id: playerItem.resource_base_id,
    position: playerItem.position,
    rarity: playerItem.rarity,
    version: playerItem.metal,
    rating: playerItem.rating
})

const prettyPrintPlayerData = (playerData) => `${playerData.name} ${playerData.position} (${playerData.rating}, ${playerData.rarity} ${playerData.version}) - ${playerData.count} occurences`;

const hashPlayerData = (playerData) => `${playerData.player_id}-${playerData.position}`;

const hashSpecificSolution = (players) => players.sort((a, b) => a.player_id - b.player_id).map(hashPlayerData).join('-');

const getPlayersInSolution = async (sbc_id, hybrid_id = 4) => {
    let req_body = {
        "id": sbc_id.toString(),
        "hybrid": false,
        "untradeable": false,
        "excludePosModifiers": false,
    }
    let headers = {
        responseType: 'json'
    }
    return gotScraping.post(conf.easy_sbcs_url, {json: req_body, headers: headers})
        .then(({body}) =>  JSON.parse(body))
        .then(({players, solution_message}) => {
            if (!solution_message && !players) throw new Error('Error response from request');
            if (solution_message && !players.length) throw new Error('No players found');

            players_data = players.map(extractPlayerData);

            if (!(sbc_id in seen_solutions)) seen_solutions[sbc_id] = {}

            specific_solution_hash = hashSpecificSolution(players_data)
            if (specific_solution_hash in seen_solutions[sbc_id]) throw new Error('Solution already seen')

            seen_solutions[sbc_id][specific_solution_hash] = players_data.map(player => player.name).join(';')

            return players_data;
        })
}

const _dumpToFile = async (filename, ext, body, encoding = 'utf8') => fs.writeFile(`${ext}/${filename}.${ext}`, body, encoding);

const dumpJsonToFile = async (filename, json) => _dumpToFile(filename, 'json', JSON.stringify(json, null, 2));

const dumpTextToFile = async(filename, text) => _dumpToFile(filename, 'txt', text);

const processSBC = async(progress_bar, sample_size, sbc_name, sbc_id, counterMap = {}) => {
    progress_bar.addTask(sbc_name, { type: 'percentage' });
    const percentage_progress = 1 / sample_size;
    for (let sample = 0; sample < sample_size; sample++) {
        await getPlayersInSolution(sbc_id).then(players => {
            players.reduce((accumulatedMap, player) => {
                let currHash = hashPlayerData(player);
                if (!(currHash in accumulatedMap)) {
                    accumulatedMap[currHash] = {
                        ...player,
                        count: 0
                    }
                }
                accumulatedMap[currHash].count++;
                return accumulatedMap;
            }, counterMap);
            
            progress_bar.incrementTask(sbc_name, { percentage: percentage_progress });
        }).catch(e => {
            console.warn(e);
            console.log('Retrying...')
            sample--;
        })
        await new Promise(resolve => setTimeout(resolve, conf.sleep_interval_seconds * 1000));
    }
    
    let ordered = Heap.nlargest(Object.values(counterMap), Object.keys(counterMap).length, playerComparator);
    const sbc_file_name = sbc_name.toLowerCase().replaceAll(' ', '_');
    await Promise.all([
        dumpJsonToFile(sbc_file_name, {write_ts: new Date().toISOString(), players: ordered}),
        dumpTextToFile(sbc_file_name, ordered.map(prettyPrintPlayerData).join('\n')),
        dumpJsonToFile('seen_solutions', seen_solutions)
    ])
    progress_bar.done(sbc_name, { message: 'Sampling done.' });
}

// Main
(async () => {
    const multi_bar = new MultiProgressBars({
        initMessage: ' $ SBC Fodder Brute Force Finder ',
        anchor: 'top',
        persist: true,
        border: true,
    });
    await Promise.all([
        ...sbc_maps.map(async sbc => {
            await processSBC(
                multi_bar, 
                conf.sample_size, 
                sbc.sbc_name, 
                sbc.id
            );
        }),
        await multi_bar.promise
    ]);
})();